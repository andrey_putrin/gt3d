
// Generated from geometry.g4 by ANTLR 4.7.2


#include "geometryListener.h"

#include "geometryParser.h"


using namespace antlrcpp;
using namespace antlr4;

geometryParser::geometryParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

geometryParser::~geometryParser() {
  delete _interpreter;
}

std::string geometryParser::getGrammarFileName() const {
  return "geometry.g4";
}

const std::vector<std::string>& geometryParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& geometryParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- FigContext ------------------------------------------------------------------

geometryParser::FigContext::FigContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* geometryParser::FigContext::FORM() {
  return getToken(geometryParser::FORM, 0);
}


size_t geometryParser::FigContext::getRuleIndex() const {
  return geometryParser::RuleFig;
}

void geometryParser::FigContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFig(this);
}

void geometryParser::FigContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFig(this);
}

geometryParser::FigContext* geometryParser::fig() {
  FigContext *_localctx = _tracker.createInstance<FigContext>(_ctx, getState());
  enterRule(_localctx, 0, geometryParser::RuleFig);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(12);
    match(geometryParser::FORM);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ColorContext ------------------------------------------------------------------

geometryParser::ColorContext::ColorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* geometryParser::ColorContext::COLOR() {
  return getToken(geometryParser::COLOR, 0);
}


size_t geometryParser::ColorContext::getRuleIndex() const {
  return geometryParser::RuleColor;
}

void geometryParser::ColorContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterColor(this);
}

void geometryParser::ColorContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitColor(this);
}

geometryParser::ColorContext* geometryParser::color() {
  ColorContext *_localctx = _tracker.createInstance<ColorContext>(_ctx, getState());
  enterRule(_localctx, 2, geometryParser::RuleColor);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(14);
    match(geometryParser::COLOR);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- XContext ------------------------------------------------------------------

geometryParser::XContext::XContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* geometryParser::XContext::NUMBER() {
  return getToken(geometryParser::NUMBER, 0);
}


size_t geometryParser::XContext::getRuleIndex() const {
  return geometryParser::RuleX;
}

void geometryParser::XContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterX(this);
}

void geometryParser::XContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitX(this);
}

geometryParser::XContext* geometryParser::x() {
  XContext *_localctx = _tracker.createInstance<XContext>(_ctx, getState());
  enterRule(_localctx, 4, geometryParser::RuleX);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(16);
    match(geometryParser::NUMBER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- YContext ------------------------------------------------------------------

geometryParser::YContext::YContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* geometryParser::YContext::NUMBER() {
  return getToken(geometryParser::NUMBER, 0);
}


size_t geometryParser::YContext::getRuleIndex() const {
  return geometryParser::RuleY;
}

void geometryParser::YContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterY(this);
}

void geometryParser::YContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitY(this);
}

geometryParser::YContext* geometryParser::y() {
  YContext *_localctx = _tracker.createInstance<YContext>(_ctx, getState());
  enterRule(_localctx, 6, geometryParser::RuleY);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(18);
    match(geometryParser::NUMBER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FuncContext ------------------------------------------------------------------

geometryParser::FuncContext::FuncContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

geometryParser::FigContext* geometryParser::FuncContext::fig() {
  return getRuleContext<geometryParser::FigContext>(0);
}

tree::TerminalNode* geometryParser::FuncContext::LPAREN() {
  return getToken(geometryParser::LPAREN, 0);
}

geometryParser::ColorContext* geometryParser::FuncContext::color() {
  return getRuleContext<geometryParser::ColorContext>(0);
}

std::vector<tree::TerminalNode *> geometryParser::FuncContext::COMMA() {
  return getTokens(geometryParser::COMMA);
}

tree::TerminalNode* geometryParser::FuncContext::COMMA(size_t i) {
  return getToken(geometryParser::COMMA, i);
}

geometryParser::XContext* geometryParser::FuncContext::x() {
  return getRuleContext<geometryParser::XContext>(0);
}

geometryParser::YContext* geometryParser::FuncContext::y() {
  return getRuleContext<geometryParser::YContext>(0);
}

tree::TerminalNode* geometryParser::FuncContext::RPAREN() {
  return getToken(geometryParser::RPAREN, 0);
}

tree::TerminalNode* geometryParser::FuncContext::SEMI() {
  return getToken(geometryParser::SEMI, 0);
}


size_t geometryParser::FuncContext::getRuleIndex() const {
  return geometryParser::RuleFunc;
}

void geometryParser::FuncContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunc(this);
}

void geometryParser::FuncContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunc(this);
}

geometryParser::FuncContext* geometryParser::func() {
  FuncContext *_localctx = _tracker.createInstance<FuncContext>(_ctx, getState());
  enterRule(_localctx, 8, geometryParser::RuleFunc);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(20);
    fig();
    setState(21);
    match(geometryParser::LPAREN);
    setState(22);
    color();
    setState(23);
    match(geometryParser::COMMA);
    setState(24);
    x();
    setState(25);
    match(geometryParser::COMMA);
    setState(26);
    y();
    setState(27);
    match(geometryParser::RPAREN);
    setState(28);
    match(geometryParser::SEMI);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpContext ------------------------------------------------------------------

geometryParser::ExpContext::ExpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<geometryParser::FuncContext *> geometryParser::ExpContext::func() {
  return getRuleContexts<geometryParser::FuncContext>();
}

geometryParser::FuncContext* geometryParser::ExpContext::func(size_t i) {
  return getRuleContext<geometryParser::FuncContext>(i);
}


size_t geometryParser::ExpContext::getRuleIndex() const {
  return geometryParser::RuleExp;
}

void geometryParser::ExpContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExp(this);
}

void geometryParser::ExpContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<geometryListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExp(this);
}

geometryParser::ExpContext* geometryParser::exp() {
  ExpContext *_localctx = _tracker.createInstance<ExpContext>(_ctx, getState());
  enterRule(_localctx, 10, geometryParser::RuleExp);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(30);
    func();
    setState(34);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == geometryParser::FORM) {
      setState(31);
      func();
      setState(36);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> geometryParser::_decisionToDFA;
atn::PredictionContextCache geometryParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN geometryParser::_atn;
std::vector<uint16_t> geometryParser::_serializedATN;

std::vector<std::string> geometryParser::_ruleNames = {
  "fig", "color", "x", "y", "func", "exp"
};

std::vector<std::string> geometryParser::_literalNames = {
  "", "", "", "", "", "','", "'('", "')'", "';'"
};

std::vector<std::string> geometryParser::_symbolicNames = {
  "", "COLOR", "NUMBER", "FORM", "WS", "COMMA", "LPAREN", "RPAREN", "SEMI"
};

dfa::Vocabulary geometryParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> geometryParser::_tokenNames;

geometryParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0xa, 0x28, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 0x9, 
    0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 0x3, 
    0x2, 0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x5, 0x3, 
    0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 
    0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x7, 0x3, 0x7, 0x7, 0x7, 0x23, 
    0xa, 0x7, 0xc, 0x7, 0xe, 0x7, 0x26, 0xb, 0x7, 0x3, 0x7, 0x2, 0x2, 0x8, 
    0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0x2, 0x2, 0x2, 0x22, 0x2, 0xe, 0x3, 0x2, 
    0x2, 0x2, 0x4, 0x10, 0x3, 0x2, 0x2, 0x2, 0x6, 0x12, 0x3, 0x2, 0x2, 0x2, 
    0x8, 0x14, 0x3, 0x2, 0x2, 0x2, 0xa, 0x16, 0x3, 0x2, 0x2, 0x2, 0xc, 0x20, 
    0x3, 0x2, 0x2, 0x2, 0xe, 0xf, 0x7, 0x5, 0x2, 0x2, 0xf, 0x3, 0x3, 0x2, 
    0x2, 0x2, 0x10, 0x11, 0x7, 0x3, 0x2, 0x2, 0x11, 0x5, 0x3, 0x2, 0x2, 
    0x2, 0x12, 0x13, 0x7, 0x4, 0x2, 0x2, 0x13, 0x7, 0x3, 0x2, 0x2, 0x2, 
    0x14, 0x15, 0x7, 0x4, 0x2, 0x2, 0x15, 0x9, 0x3, 0x2, 0x2, 0x2, 0x16, 
    0x17, 0x5, 0x2, 0x2, 0x2, 0x17, 0x18, 0x7, 0x8, 0x2, 0x2, 0x18, 0x19, 
    0x5, 0x4, 0x3, 0x2, 0x19, 0x1a, 0x7, 0x7, 0x2, 0x2, 0x1a, 0x1b, 0x5, 
    0x6, 0x4, 0x2, 0x1b, 0x1c, 0x7, 0x7, 0x2, 0x2, 0x1c, 0x1d, 0x5, 0x8, 
    0x5, 0x2, 0x1d, 0x1e, 0x7, 0x9, 0x2, 0x2, 0x1e, 0x1f, 0x7, 0xa, 0x2, 
    0x2, 0x1f, 0xb, 0x3, 0x2, 0x2, 0x2, 0x20, 0x24, 0x5, 0xa, 0x6, 0x2, 
    0x21, 0x23, 0x5, 0xa, 0x6, 0x2, 0x22, 0x21, 0x3, 0x2, 0x2, 0x2, 0x23, 
    0x26, 0x3, 0x2, 0x2, 0x2, 0x24, 0x22, 0x3, 0x2, 0x2, 0x2, 0x24, 0x25, 
    0x3, 0x2, 0x2, 0x2, 0x25, 0xd, 0x3, 0x2, 0x2, 0x2, 0x26, 0x24, 0x3, 
    0x2, 0x2, 0x2, 0x3, 0x24, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

geometryParser::Initializer geometryParser::_init;
