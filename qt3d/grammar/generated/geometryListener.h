
// Generated from geometry.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "geometryParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by geometryParser.
 */
class  geometryListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterFig(geometryParser::FigContext *ctx) = 0;
  virtual void exitFig(geometryParser::FigContext *ctx) = 0;

  virtual void enterColor(geometryParser::ColorContext *ctx) = 0;
  virtual void exitColor(geometryParser::ColorContext *ctx) = 0;

  virtual void enterX(geometryParser::XContext *ctx) = 0;
  virtual void exitX(geometryParser::XContext *ctx) = 0;

  virtual void enterY(geometryParser::YContext *ctx) = 0;
  virtual void exitY(geometryParser::YContext *ctx) = 0;

  virtual void enterFunc(geometryParser::FuncContext *ctx) = 0;
  virtual void exitFunc(geometryParser::FuncContext *ctx) = 0;

  virtual void enterExp(geometryParser::ExpContext *ctx) = 0;
  virtual void exitExp(geometryParser::ExpContext *ctx) = 0;


};

