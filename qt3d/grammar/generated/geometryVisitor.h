
// Generated from geometry.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "geometryParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by geometryParser.
 */
class  geometryVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by geometryParser.
   */
    virtual antlrcpp::Any visitFig(geometryParser::FigContext *context) = 0;

    virtual antlrcpp::Any visitColor(geometryParser::ColorContext *context) = 0;

    virtual antlrcpp::Any visitX(geometryParser::XContext *context) = 0;

    virtual antlrcpp::Any visitY(geometryParser::YContext *context) = 0;

    virtual antlrcpp::Any visitFunc(geometryParser::FuncContext *context) = 0;

    virtual antlrcpp::Any visitExp(geometryParser::ExpContext *context) = 0;


};

