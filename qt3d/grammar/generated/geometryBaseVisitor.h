
// Generated from geometry.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "geometryVisitor.h"


/**
 * This class provides an empty implementation of geometryVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  geometryBaseVisitor : public geometryVisitor {
public:

  virtual antlrcpp::Any visitFig(geometryParser::FigContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitColor(geometryParser::ColorContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitX(geometryParser::XContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitY(geometryParser::YContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunc(geometryParser::FuncContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExp(geometryParser::ExpContext *ctx) override {
    return visitChildren(ctx);
  }


};

