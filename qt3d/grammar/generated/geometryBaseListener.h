
// Generated from geometry.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "geometryListener.h"


/**
 * This class provides an empty implementation of geometryListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  geometryBaseListener : public geometryListener {
public:

  virtual void enterFig(geometryParser::FigContext * /*ctx*/) override { }
  virtual void exitFig(geometryParser::FigContext * /*ctx*/) override { }

  virtual void enterColor(geometryParser::ColorContext * /*ctx*/) override { }
  virtual void exitColor(geometryParser::ColorContext * /*ctx*/) override { }

  virtual void enterX(geometryParser::XContext * /*ctx*/) override { }
  virtual void exitX(geometryParser::XContext * /*ctx*/) override { }

  virtual void enterY(geometryParser::YContext * /*ctx*/) override { }
  virtual void exitY(geometryParser::YContext * /*ctx*/) override { }

  virtual void enterFunc(geometryParser::FuncContext * /*ctx*/) override { }
  virtual void exitFunc(geometryParser::FuncContext * /*ctx*/) override { }

  virtual void enterExp(geometryParser::ExpContext * /*ctx*/) override { }
  virtual void exitExp(geometryParser::ExpContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

