grammar geometry;

fig: FORM;
color: COLOR;
x: NUMBER;
y: NUMBER;

func: fig LPAREN color COMMA x COMMA y RPAREN SEMI;
exp: func (func)*;

COLOR: ('red' | 'green');
NUMBER: [0-9]+;
FORM: ('circle' | 'rect');
WS : [ \t\r\n]+ -> skip ;
COMMA: ',';
LPAREN: '(';
RPAREN: ')';
SEMI: ';';
