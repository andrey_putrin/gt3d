#include "mainwindow.h"
#include "sceneparser.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::setAsd);
    ui->textEdit->setText("circle(red,10,2);");
}

MainWindow::~MainWindow()
{
    delete ui;
}

//void MainWindow::on_pushButton_clicked()
void MainWindow::setAsd()
{
//    ui->textEdit->setText("Pressed");
}

void MainWindow::parseScene() {
    try {
        auto t = ui->textEdit->toPlainText();
        auto a = t.toStdString();
        auto s = SceneParser::ParseScene(a);
        ui->widget->setScene(s);
    }
    catch(...) {

    }
}

void MainWindow::on_actionToolAndMenu_triggered()
{
    parseScene();
}

void MainWindow::on_textEdit_textChanged()
{
    parseScene();
}
