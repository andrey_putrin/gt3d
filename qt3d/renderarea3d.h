#ifndef RENDERAREA3D_H
#define RENDERAREA3D_H

#include <QtOpenGL/QGLWidget>

class RenderArea3D : public QGLWidget
{
    Q_OBJECT
public:
    explicit RenderArea3D(QWidget *parent = nullptr);

signals:
public slots:
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    int xRot;
    int yRot;
    int zRot;

    QPoint lastPos;

    void draw();
};

#endif // RENDERAREA3D_H
