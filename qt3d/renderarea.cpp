#include <QPainter>
#include "renderarea.h"

RenderArea::RenderArea(QWidget* parent)
    :   QWidget (parent)
{

}

void RenderArea::paintEvent(QPaintEvent*) {
    if (scene.isEmpty()) return;

    QPainter painter(this);
    painter.setBrush(QBrush(Qt::red));
    for(auto a : scene.circles) {
        painter.drawRect(a.X,a.Y,50,50);
    }
}
