#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
//    void on_pushButton_clicked();
    void setAsd();

    void on_actionToolAndMenu_triggered();

    void on_textEdit_textChanged();

private:
    void parseScene();

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
