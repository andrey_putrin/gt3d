#ifndef SCENEPARSER_H
#define SCENEPARSER_H

#include <string>
#include "scene.h"

class SceneParser
{
public:
    static Scene ParseScene(const std::string& in);
};

#endif // SCENEPARSER_H
