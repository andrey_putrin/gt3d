#include "sceneparser.h"
#include "antlr4-runtime.h"
#include "geometryLexer.h"
#include "geometryParser.h"
#include "geometryVisitor.h"

Scene SceneParser::ParseScene(const std::string& in)
{
    antlr4::ANTLRInputStream input(in);
    geometryLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);
    geometryParser parser(&tokens);

    geometryParser::ExpContext* tree = parser.exp();
    Scene result;
    for(auto f: tree->func()) {
        result.circles.push_back(Circle(
                                     std::stoi(f->x()->getText()),
                                     std::stoi(f->y()->getText())
                                     ));
    }
    return result;
}
