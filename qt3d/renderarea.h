#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>
#include <memory>
#include "scene.h"

class RenderArea: public QWidget
{
public:
    RenderArea(QWidget* parent = nullptr);
    void setScene(const Scene& s) {
        scene = s;
        this->update();
    }
protected:
    void paintEvent(QPaintEvent* event) override;
private:
    Scene scene;
};

#endif // RENDERAREA_H
