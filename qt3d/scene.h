#ifndef SCENE_H
#define SCENE_H

#include <vector>

struct Circle {
    int X;
    int Y;

    Circle(int x, int y) {
        X = x;
        Y = y;
    }
};

struct Scene {
    std::vector<Circle> circles;

    bool isEmpty() const {
        return circles.empty();
    }
};

#endif // SCENE_H
